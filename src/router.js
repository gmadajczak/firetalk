import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import About from './views/About.vue';
import VueHistory from './views/Vue_History.vue';
import VueHistory2 from './views/Vue_History-2.vue';
import AngularHistory from './views/Angular_History.vue';
import AngularHistory2 from './views/Angular_History-2.vue';
import Slide6 from './views/Slide6.vue';
import Slide7 from './views/Slide7.vue';
import Slide8 from './views/Slide8.vue';
import Slide9 from './views/Slide9.vue';
import Slide10 from './views/Slide10.vue';
import Slide11 from './views/Slide11.vue';
import Settings from './views/Settings.vue';

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: { next: true },
    },
    {
      path: '/slide-1',
      name: 'about',
      component: About,
      meta: { next: true, prev: true },
    },
    {
      path: '/slide-2',
      name: 'slide-2',
      component: AngularHistory,
      meta: { down: true, prev: true },
    },
    {
      path: '/slide-3',
      name: 'slide-3',
      component: AngularHistory2,
      meta: { up: true, next: true },
    },
    {
      path: '/slide-4',
      name: 'slide-4',
      component: VueHistory,
      meta: { down: true, prev: true },
    },
    {
      path: '/slide-5',
      name: 'slide-5',
      component: VueHistory2,
      meta: { next: true, up: true },
    },
    {
      path: '/slide-6',
      name: 'slide-6',
      component: Slide6,
      meta: { next: true, prev: true },
    },
    {
      path: '/slide-7',
      name: 'slide-7',
      component: Slide7,
      meta: { next: true, prev: true },
    },
    {
      path: '/slide-8',
      name: 'slide-8',
      component: Slide8,
      meta: { next: true, prev: true },
    },
    {
      path: '/slide-9',
      name: 'slide-9',
      component: Slide9,
      meta: { next: true, prev: true },
    },
    {
      path: '/slide-10',
      name: 'slide-10',
      component: Slide10,
      meta: { next: true, prev: true },
    },
    {
      path: '/slide-11',
      name: 'slide-11',
      component: Slide11,
      meta: { prev: true },
    },
    {
      path: '/settings',
      name: 'settings',
      component: Settings,
    },
    {
      path: '*',
      redirect: '/',
    },
  ],
});

export default router;
