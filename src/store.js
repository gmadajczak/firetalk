/**
 * Created by madajczak@gmail.com on 12.11.2017.
 */
import Vuex from 'vuex';
import Vue from 'vue';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    transitionName: 'slide-prev',
    projectId: '',
    master: false,
  },
  mutations: {
    'transition-name': (state, name) => {
      state.transitionName = name;
    },
    'set-project-id': (state, projectId) => {
      state.projectId = projectId;
    },
    'set-master': (state, master) => {
      state.master = master;
    },
  },
});

export default store;
