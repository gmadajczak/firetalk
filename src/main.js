import Vue from 'vue';
import VueHighlightJS from 'vue-highlightjs';

import App from './App.vue';
import router from './router';
import './registerServiceWorker';
import store from './store';
import Nav from './components/Nav.vue';

require('./sass/style.scss');

Vue.config.productionTip = false;
Vue.use(VueHighlightJS);

Vue.component('slide-nav', Nav);
new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
